﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmsNewsService.Core;
using SmsNewsService.Core.Models;

namespace TestProject
{
    class Program
    {
        private const string ApiId = "98377240-E9E7-3BAF-C190-8F2F541023D4";

        private const string Phone = "79519357665";

        private const int MaxMessageLength = 160;

        private readonly SmsClient smsClient = new SmsClient(ApiId);
        static void Main(string[] args)
        {
            var logEntries = new List<string>();

            logEntries.Add("Получение информации о количестве оставшихся сообщений...");
            var smsClient = new SmsClient(ApiId);
            var freeSmsStatus = smsClient.GetFreeSmsStatus();
            if (freeSmsStatus.StatusCode != 100)
            {
                logEntries.Add("Ошибка при получении количества бесплатных сообщений. Отмена операции.");
            }
            else
            {
                var remaining = (freeSmsStatus.TotalFree ?? 0) - (freeSmsStatus.UsedToday ?? 0);
                logEntries.Add($"Осталось (({remaining})) бесплатных СМС на сегодня.");
                if (remaining > 0)
                {
                    var news = new NewsFetcher().GetShortNews(remaining).ToList();
                    logEntries.Add($"Получено (({news.Count})) новостей.");
                    foreach (var n in news)
                    {
                        var cut = n.Substring(0, Math.Min(n.Length, MaxMessageLength));
                        var message = new Message
                        {
                            Receiver = Phone,
                            Text = cut,
                            Translit = true
                        };
                        logEntries.Add($"Попытка послать сообщение (({cut.Substring(cut.Length, 10)}...)) по номеру (({Phone}))...");
                        var sendSmsResponse = smsClient.SendSms(message, true);
                        if (sendSmsResponse.StatusCode == 100)
                        {
                            logEntries.Add("Сообщение успешно отправлено!");
                        }
                        else
                        {
                            logEntries.Add("Ошибка при отправке сообщения");
                        }
                    }
                }
            }

            Console.WriteLine(string.Join("\n", logEntries));
        }
    }
}
