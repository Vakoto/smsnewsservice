﻿using Newtonsoft.Json;
using RestSharp;
using SmsNewsService.Core.Helpers;
using SmsNewsService.Core.Models;

namespace SmsNewsService.Core
{
    public class SmsClient
    {
        private const string smsUrl = "https://sms.ru";

        private readonly string apiId;

        private readonly RestClient client = new RestClient(smsUrl);

        public SmsClient(string apiId)
        {
            Guard.ArgumentNotNullOrEmpty(apiId, nameof(apiId));

            var authStatus = CheckApiId(apiId);
            Guard.ArgumentValid(authStatus.StatusCode == 100, "Нерабочий api_id", nameof(apiId));

            this.apiId = apiId;
        }

        private AuthStatus CheckApiId(string apiId)
        {
            var request = new RestRequest($"/auth/check", Method.GET)
                .AddParameter("api_id", apiId)
                .AddParameter("json", 1);

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<AuthStatus>(response.Content);
        }

        public FreeSmsStatus GetFreeSmsStatus()
        {
            var request = new RestRequest($"/my/free", Method.GET)
                .AddParameter("api_id", apiId)
                .AddParameter("json", 1);

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<FreeSmsStatus>(response.Content);
        }

        private int BoolToInt(bool b)
        {
            return b ? 1 : 0;
        }

        public SendSmsResponse SendSms(Message message, bool isTestRun)
        {
            Guard.ArgumentNotNull(message, nameof(message));

            var request = new RestRequest($"/sms/send", Method.GET)
                .AddParameter("api_id", apiId)
                .AddParameter("json", 1)
                .AddParameter("to", message.Receiver)
                .AddParameter("msg", message.Text)
                .AddParameter("translit", BoolToInt(message.Translit))
                .AddParameter("test", BoolToInt(isTestRun));

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<SendSmsResponse>(response.Content);
        }
    }
}
