﻿namespace SmsNewsService.Core.Models
{
    public class Message
    {
        public string Receiver { get; set; }

        public string Text { get; set; }

        public bool Translit { get; set; }
    }
}
