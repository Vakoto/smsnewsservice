﻿using Newtonsoft.Json;

namespace SmsNewsService.Core.Models
{
    [JsonObject]
    public class AuthStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int? StatusCode { get; set; }

        [JsonConstructor]
        private AuthStatus()
        {
        }
    }
}
