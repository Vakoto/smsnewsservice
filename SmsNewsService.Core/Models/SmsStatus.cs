﻿using Newtonsoft.Json;

namespace SmsNewsService.Core.Models
{
    [JsonObject]
    public class SmsStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int StatusCode { get; set; }

        [JsonProperty("sms_id")]
        public string SmsId { get; set; }

        [JsonProperty("cost")]
        public string Cost { get; set; }

        [JsonConstructor]
        private SmsStatus()
        {
        }
    }
}
