﻿using Newtonsoft.Json;

namespace SmsNewsService.Core.Models
{
    [JsonObject]
    public class FreeSmsStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int? StatusCode { get; set; }

        [JsonProperty("total_free")]
        public int? TotalFree { get; set; }

        [JsonProperty("used_today")]
        public int? UsedToday { get; set; }

        [JsonConstructor]
        private FreeSmsStatus()
        {
        }
    }
}
