﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmsNewsService.Core.Models
{
    [JsonObject]
    public class SendSmsResponse
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int? StatusCode { get; set; }

        [JsonProperty("sms")]
        public Dictionary<string, SmsStatus> SmsStatuses { get; set; }

        [JsonProperty("balance")]
        public double? Balance { get; set; }

        [JsonConstructor]
        private SendSmsResponse()
        {
        }
    }
}
