﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;
using SmsNewsService.Core.Helpers;

namespace SmsNewsService.Core
{
    public class NewsFetcher
    {
        public static readonly string[] DefaultNewsSources =
        {
            "https://lenta.ru/rss/news",
            "https://lenta.ru/rss/top7",
            "https://lenta.ru/rss/last24"
        };

        private readonly string[] newsSources;

        public NewsFetcher() : this(DefaultNewsSources)
        {
        }

        public NewsFetcher(string[] newsSources)
        {
            Guard.ArgumentNotNull(newsSources, nameof(newsSources));

            this.newsSources = newsSources;
        }

        public IEnumerable<string> GetShortNews(int maxNewsCount)
        {
            Guard.ArgumentInRange(maxNewsCount > 0, "Количество новостей должно быть больше 0", nameof(maxNewsCount));

            var count = 0;
            var random = new Random();
            foreach (var url in newsSources.OrderBy(x => random.Next()))
            {
                using (var reader = XmlReader.Create(url))
                {
                    var feed = SyndicationFeed.Load(reader);

                    foreach (var item in feed.Items)
                    {
                        if (item.Summary != null)
                        {
                            yield return item.Title.Text;

                            if (++count >= maxNewsCount)
                            {
                                yield break;
                            }
                        }
                    }
                }
            }
        }
    }
}