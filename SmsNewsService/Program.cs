﻿using System.ServiceProcess;

namespace SmsNewsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SmsNewsService(args)
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
