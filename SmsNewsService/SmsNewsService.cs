﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Runtime.InteropServices;
using SmsNewsService.Core;
using SmsNewsService.Core.Models;
using System;

namespace SmsNewsService
{
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public int dwServiceType;
        public ServiceState dwCurrentState;
        public int dwControlsAccepted;
        public int dwWin32ExitCode;
        public int dwServiceSpecificExitCode;
        public int dwCheckPoint;
        public int dwWaitHint;
    };

    public partial class SmsNewsService : ServiceBase
    {
        private const string ApiId = "98377240-E9E7-3BAF-C190-8F2F541023D4";

        private const string Phone = "79519357665";

        private const int MaxMessageLength = 160;

        private readonly SmsClient smsClient = new SmsClient(ApiId);

        private int eventId = 1;

        public SmsNewsService(string[] args)
        {
            InitializeComponent();

            string eventSourceName = "MySource";
            string logName = "MyNewLog";

            if (args.Length > 0)
            {
                eventSourceName = args[0];
            }

            if (args.Length > 1)
            {
                logName = args[1];
            }

            eventLog = new EventLog();

            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, logName);
            }

            eventLog.Source = eventSourceName;
            eventLog.Log = logName;
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            var logEntries = new List<string>();
            var entryType = EventLogEntryType.Information;

            logEntries.Add("Получение информации о количестве оставшихся сообщений...");

            var freeSmsStatus = smsClient.GetFreeSmsStatus();
            if (freeSmsStatus.StatusCode != 100)
            {
                logEntries.Add("Ошибка при получении количества бесплатных сообщений. Отмена операции.");
                entryType = EventLogEntryType.Error;
            }
            else
            {
                var remaining = (freeSmsStatus.TotalFree ?? 0) - (freeSmsStatus.UsedToday ?? 0);
                logEntries.Add($"Осталось (({remaining})) бесплатных СМС на сегодня.");
                if (remaining > 0)
                {
                    var news = new NewsFetcher().GetShortNews(remaining).ToList();
                    logEntries.Add($"Получено (({news.Count})) новостей.");
                    foreach (var n in news)
                    {
                        var cut = n.Substring(0, Math.Min(n.Length, MaxMessageLength));
                        var message = new Message
                        {
                            Receiver = Phone,
                            Text = cut,
                            Translit = true
                        };
                        logEntries.Add($"Попытка послать сообщение (({cut.Substring(0, Math.Min(cut.Length, 10))}...)) по номеру (({Phone}))...");
                        var sendSmsResponse = smsClient.SendSms(message, false);
                        if (sendSmsResponse.StatusCode == 100)
                        {
                            logEntries.Add("Сообщение успешно отправлено!");
                        }
                        else
                        {
                            logEntries.Add("Ошибка при отправке сообщения");
                            entryType = EventLogEntryType.Warning;
                        }
                    }
                }
            }

            eventLog.WriteEntry(string.Join("\n", logEntries), entryType, eventId++);
        }

        protected override void OnStart(string[] args)
        {
            var serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            eventLog.WriteEntry("SmsNewsService начала работу.");

            var timer = new Timer();
            timer.Interval = 2 * 60 * 60 * 1000; // (2 hours)
            timer.Elapsed += this.OnTimer;
            timer.Start();

            // manual tick call
            this.OnTimer(null, null);
            
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            var serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            eventLog.WriteEntry("SmsNewsService остановлена.");
            
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);
    }
}
